#!/usr/bin/python

"""
Copyright (c) 2019 Sunlight.io Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License."""

import shutil
import os

current_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.abspath(os.path.join(current_path, os.pardir))
dst = '/opt/stack/nova/nova/virt/slonova/'
print("Upload nova driver to " + dst)

try:
    shutil.rmtree(dst)
except OSError:
    print("The folder slonova does not exists, it will create new one")
finally:
    shutil.copytree(parent_path + '/slonova', dst )
