"""
Copyright (c) 2019 Sunlight.io Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License."""

import os
import time
import threading
import re
import yaml

from oslo_log import log as logging
from cinder import utils

LOG = logging.getLogger(__name__)


class NbdSafeCounterClass():

    def __init__(self):
        self.limit = 15
        self.lock = threading.Lock()
        self.counter = []
        LOG.info("NbdCnt: init")

    def get_nbd_device(self, num):
        return '/dev/nbd' + str(num)

    def remove_nbd_device(self, num):
        LOG.info("NbdCnt: remove nbd" + str(num))
        with self.lock:
            if self.exists(num):
                self.counter.remove(num)

    def exists(self, num):
        return num in self.counter

    def give_num_nbd_device(self):
        with self.lock:
            for n in range(0, self.limit):
                if not self.exists(n):
                    self.counter.append(n)
                    LOG.info("NbdCnt: give num " + str(n))
                    return n

            raise Exception("No NBD device is free")


nbdCounter = NbdSafeCounterClass()


class ImageProvisioningMutexHandler():

    def __init__(self):
        self.images = {}

    def get_mutex(self, image_id):
        if image_id not in self.images.keys():
            self.images[image_id] = threading.Lock()

        return self.images[image_id]


_image_mutex_handler = ImageProvisioningMutexHandler()


def load_image_to_volume(image_id,
                         disk_format, device):
    mutex = _image_mutex_handler.get_mutex(image_id)
    mutex.acquire()
    LOG.info("Acquired mutex for provisioning image " + image_id)
    try:
        LOG.info("Load image " + image_id + " to volume " + device)
        image_file = '/opt/stack/data/glance/images/' + image_id
        cmd = ('qemu-img', 'convert', '-t', 'directsync', '-T',
               'none', '-f', disk_format, '-O', 'raw',
               image_file, device)
        LOG.info("calling " + str(cmd))
        utils.execute(*cmd, run_as_root=True)
    finally:
        mutex.release()
        LOG.info("Released the mutex for provisioning image " + image_id)

    LOG.info("converted the image to device")


def connect_nbd(ip, port, nbd_device):
    cmd = ('nbd-client', ip, port, nbd_device)
    LOG.info("calling " + str(cmd))
    res = utils.execute(*cmd, run_as_root=True)
    LOG.info("result " + str(res))


def disconnect_nbd(nbd_device):
    cmd = ('nbd-client', '-d', nbd_device)
    LOG.info("calling " + str(cmd))
    res = utils.execute(*cmd, run_as_root=True)
    LOG.info("result " + str(res))
