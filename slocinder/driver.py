"""
Copyright (c) 2019 Sunlight.io Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License."""

import json
import time
from oslo_log import log as logging
from oslo_config import cfg
from oslo_utils import importutils

from cinder import context
from cinder.volume import api as volapi
from cinder.volume import driver

from slolibrary import http_client
from . import volutils
from oslo_config import cfg
CONF = cfg.CONF
LOG = logging.getLogger(__name__)


class SlapiDriver(driver.VolumeDriver):
    VERSION = "0.0.1"
    VENDOR = "SUNLIGHT.IO"

    def __init__(self, vg_obj=None, *args, **kwargs):
        # Parent sets db, host, _execute and base config
        super(SlapiDriver, self).__init__(*args, **kwargs)
        self._ctxt = context.get_admin_context()

        LOG.info("Starting SlapiDriver !! " + str(vg_obj))
        self.backend_name =\
            self.configuration.safe_get('volume_backend_name') or 'LVM'
        target_driver = \
            self.target_mapping[self.configuration.safe_get('iscsi_helper')] # FIXME: iscsi?
        self.target_driver = importutils.import_object(
            target_driver,
            configuration=self.configuration,
            db=self.db,
            executor=self._execute)
        self.protocol = self.target_driver.protocol

    def create_volume(self, volume):
        LOG.info("CALLS: create_volume " + str(volume.__dict__))
        # showing only the 27 characters because the storage api accepts 28 characters only as name
        name = volume.id[:27]
        http_client.create_volume(name, volume.size)

    def check_for_setup_error(self):
        """No setup currently necessary in SLAPI mode."""
        LOG.info("CALLS: " + "check_for_setup_error")

    def initialize_connection(self, volume, connector):
        LOG.info("CALLS: " + "initialize_connection")
        LOG.info("connector " + str(connector))

        return {
            'driver_volume_type': 'sunlight_storage',
            'data': {'access_mode': 'rw'},
        }

    def terminate_connection(self, volume, connector, **kwargs):
        LOG.info("CALLS: " + "terminate_connection")
        LOG.info("connector " + str(connector))

    def create_volume_from_snapshot(self, volume, snapshot):
        """Creates a volume from a snapshot."""
        LOG.info("CALLS: " + "create_volume_from_snapshot")

    def create_cloned_volume(self, volume, src_vref):
        """Creates a clone of the specified volume."""
        LOG.info("CALLS: " + "create_cloned_volume")
        LOG.info("volume: " + str(volume))
        LOG.info("src_vref : " + str(src_vref))
        http_client.clone_volume(src_vref.id[:27], volume.id[:27], volume.size)

    def delete_volume(self, volume):
        """Deletes a volume."""
        LOG.info("call delete_volume " + str(volume))
        try:
            name = volume.id[:27]
            http_client.delete_volume(name)
        except http_client.NotFoundException as ex:
            LOG.error(ex.message)

    def create_snapshot(self, snapshot):
        """Creates a snapshot."""
        LOG.info("Create snapshot " + str(snapshot))

    def delete_snapshot(self, snapshot):
        """Deletes a snapshot."""
        LOG.info("Delete snapshot " + str(snapshot))

    def local_path(self, volume):
        LOG.info("CALLS: " + "local_path")
        return '/tmp/volume-%s' % volume.id

    def ensure_export(self, context, volume):
        """Synchronously recreates an export for a volume."""
        LOG.info("CALLS: " + "ensure_export")

    def create_export(self, context, volume, connector):
        """Exports the volume. Can optionally return a Dictionary of changes
        to the volume object to be persisted.
        """
        LOG.info("CALLS: " + "create_export")

    def remove_export(self, context, volume):
        """Removes an export for a volume"""
        LOG.info("CALLS: " + "remove_export")
    
    def extend_volume(self, volume, new_size):
        LOG.info("CALLS: " + "extend_volume")

    def get_volume_stats(self, refresh=False):
        """Get volume status.

        If 'refresh' is True, run update the stats first.
        """
        LOG.info("Get_volume_stats, " + str(refresh))
        stats = {}
        stats["volume_backend_name"] = self.backend_name
        stats["vendor_name"] = self.VENDOR
        stats["driver_version"] = self.VERSION
        stats["storage_protocol"] = self.protocol
        total_capacity = 0
        free_capacity = 0
        provisioned_capacity = round(
            float(total_capacity) - float(free_capacity), 2)
        pool_name = "sunlight_pool"
        pool = dict(
            pool_name=pool_name,
            total_capacity_gb=total_capacity,
            free_capacity_gb=free_capacity,
            reserved_percentage=2,
            location_info=pool_name + "@" + "localhost",
            QoS_support=False,
            provisioned_capacity_gb=provisioned_capacity,
            max_over_subscription_ratio=(
                self.configuration.max_over_subscription_ratio), # does this exist? shoule set to 0?
            thin_provisioning_support=False,
            thick_provisioning_support=True,
            total_volumes=1000, # max volumes allowed
            filter_function=self.get_filter_function(),
            goodness_function=self.get_goodness_function()
        )
        stats["pools"] = [pool]
        return stats

    def get_filter_function(self):
        LOG.info("CALLS: " + "get_filter_function")
        return None

    def get_goodness_function(self):
        LOG.info("CALLS: " + "get_goodness_function")
        ret_function = self.configuration.goodness_function
        if not ret_function:
            ret_function = CONF.goodness_function
        if not ret_function:
            ret_function = self.get_default_goodness_function()
        return ret_function

    def get_default_filter_function(self):
        LOG.info("CALLS: " + "get_default_filter_function")
        return None

    def get_default_goodness_function(self):
        LOG.info("CALLS: " + "get_default_goodness_function")
        return None

    def get_pool(self, volume):
        LOG.info("CALLS: " + "get_pool")
        return self.backend_name

    def copy_volume_to_image(self, context, volume,
                             image_service, image_id):
        """Copy the volume to the specified image."""
        LOG.info("CALLS: " + "copy_volume_to_image")

    def attach_volume(self, context, volume, instance_uuid, host_name,
                      mountpoint):
        """Callback for volume attached to instance or host."""
        LOG.info("CALLS: " + "attach_volume")

    def detach_volume(self, context, volume, attachment=None):
        """Callback for volume detached."""
        LOG.info("CALLS: " + "detach_volume")

    def backup_volume(self, context, backup, backup_service):
        """Create a new backup from an existing volume."""
        LOG.info("CALLS: " + "backup_volume")

    def clear_download(self, context, volume):
        """Clean up after an interrupted image copy."""
        LOG.info("CALLS: " + "clear_download")

    def clone_image(self, context, volume,
                    image_location, image_meta,
                    image_service):
        LOG.info("CALLS: " + "clone_image")
        LOG.info("Image location : " + str(image_location))
        LOG.info("Image meta : " + str(image_meta))
        LOG.info("Image service : " + str(image_service))
        return None, False

    def copy_volume_data(self, context, src_vol, dest_vol, remote=None):
        LOG.info("CALLS: " + "copy_volume_data")

    def do_setup(self, context):
        """Any initialization the volume driver does while starting."""
        LOG.info("CALLS: " + "do_setup")

    def validate_connector(self, connector):
        """Fail if connector doesn't contain all the data needed by driver."""
        LOG.info("CALLS: " + "validate_connector")

    def secure_file_operations_enabled(self):
        """Determine if driver is running in Secure File Operations mode.

        The Cinder Volume driver needs to query if this driver is running
        in a secure file operations mode. By default, it is False: any driver
        that does support secure file operations should override this method.
        """
        LOG.info("CALLS: " + "secure_file_operations_enabled")
        return False

    def copy_image_to_volume(self, context, volume,
                             image_service, image_id):
        """Fetch the image from image_service and write it to the volume."""
        LOG.info("CALLS: " + "copy_image_to_volume")
        image_dict = image_service.show(context, image_id)
        LOG.info("image: " + str(image_dict))
        disk_format = image_dict['disk_format']
        # showing only the 27 characters because the storage api accepts 28 characters only as name
        name = volume.id[:27]
        res = http_client.attach_nbd_volume(name)
        time.sleep(3)
        LOG.info("Result from attach: " + str(res))
        num = volutils.nbdCounter.give_num_nbd_device()
        device = volutils.nbdCounter.get_nbd_device(num)
        try:
            volutils.connect_nbd(CONF.slapi.connection, res['port'], device)
            time.sleep(2)
            volutils.load_image_to_volume(image_id, disk_format, device)
        finally:
            try:
                volutils.disconnect_nbd(device)
            finally:
                http_client.detach_nbd_volume(name)
                volutils.nbdCounter.remove_nbd_device(num)
