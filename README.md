# OpenStack Pike drivers for the Sunlight.io Platform

Copyright (c) 2019 Sunlight.io Ltd.

---

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

## Introduction

The Sunlight.io platform is controlled through an intuitive web-based user interface (UI) and also provides documented command-line (CLI) tools and a native Sunlight.io REST API for managing resources and instances on the infrastructure.  
To allow integration with external management and orchestration services, such as OpenStack, the Sunlight.io platform also offers an additional set of external APIs. SunLight API (SLAPI), one of the external APIs, provides a list of endpoints that allow communication and control of the Sunlight.io NexVisors through an OpenStack platform.  
This document describes the steps for installing the SLAPI drivers in a standard OpenStack Pike distribution like devstack.

## Installation steps:

### 1. Check access to SLAPI on the Sunlight.io controller
  $ curl --insecure  -d '{"username":"< Sunlight admin’s username >", "password":"< Sunlight admin’s password >"}' -H "Content-Type: application/json" -X POST https://< sunlight controller IP >/auth/login 

### 2. Create user ‘stack’ and use it for installing devstack
  $ sudo useradd -s /bin/bash -d /opt/stack -m stack
  
  $ echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
  
  $ sudo su - stack
  
  $ pwd  
  /opt/stack

### 3. Download devstack
  $ git clone https://git.openstack.org/openstack-dev/devstack  
  Cloning into 'devstack'...  
  remote: Enumerating objects: 44200, done.  
  remote: Counting objects: 100% (44200/44200), done.  

### 4. Install the Pike version of the openstack this will take about an hour
  $ cd devstack && git checkout stable/pike && ./stack.sh && cd

### 5. Download SLAPI openstack’s drivers
  $ git clone https://bitbucket.org/sunlightio/openstack_slapi_drivers 
  
  $ cd openstack_slapi_drivers

### 6. Install drivers
  $ sudo ./install-drivers.sh

### 7. Execute the script that will prepare the openstack configurations
  $ ~/devstack/clean.sh && sudo ./install-everything.py --controller=192.168.10.118 --os_password="< external openstack services' common password >" --sl_username="< Sunlight admin’s username >" --sl_password="< Sunlight admin’s password >"

### 8. Restart external openstack services 
  $ sudo ./restart-services.sh


### 9. Reboot the VM to enable the NBD devices
  $ reboot

### 10. Install NBD client for provisioning volumes from the external openstack
  $ wget http://launchpadlibrarian.net/166862192/nbd-client_3.7-1_amd64.deb -O /tmp/nbd-client_3.7-1_amd64.deb
  
  $ sudo dpkg -i /tmp/nbd-client_3.7-1_amd64.deb

---

## Acknowledgements

Development of this software has been supported by the EU Horizon 2020 research and innovation programme under Grant Agreement no. 732366 for the ACTiCLOUD project (https://acticloud.eu/).

