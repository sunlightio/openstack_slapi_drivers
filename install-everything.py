#!/usr/bin/python

"""
Copyright (c) 2019 Sunlight.io Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License."""

from keystoneauth1 import identity
from keystoneauth1 import session
from neutronclient.v2_0 import client
import argparse
import requests
import sys
import os
from string import Template

parser = argparse.ArgumentParser(
    description="Install sunlight's drivers and configurations for devstack")

parser.add_argument('--controller', action="store",
                    dest="controller", type=str)

parser.add_argument('--os_password', action="store",
                    dest="os_password", type=str)

parser.add_argument('--sl_username', action="store",
                    dest="sl_username", type=str)

parser.add_argument('--sl_password', action="store",
                    dest="sl_password", type=str)


# create the local.conf and save it in "/opt/stack/devstack"
args = parser.parse_args()


def validate_sub_args():
    if not args.controller:
        print("Error: add the IP of the controller.")
        sys.exit(1)

    if not args.os_password:
        print("Error: add a password for the Openstack.")
        sys.exit(1)

    if not args.sl_username:
        print("Error: add username for the Sunlight API.")
        sys.exit(1)

    if not args.sl_password:
        print("Error: add password for the Sunlight API.")
        sys.exit(1)


validate_sub_args()

CONTROLLER = args.controller
OS_PASSWORD = args.os_password
SL_USERNAME = args.sl_username
SL_PASSWORD = args.sl_password
DEVSTACK_FOLDER = "/opt/stack/devstack"
SCRIPT_FOLDER = os.path.dirname(os.path.realpath(__file__))

print("Login")
payload = {'username': SL_USERNAME, 'password': SL_PASSWORD}
r = requests.post("https://" + CONTROLLER + "/auth/login", json=payload, verify=False)
user = r.json()
token = user["jwt"]
headers = {'Authorization': "Bearer " + token}

print("Check if there are hypervisors prepared.")
url = "https://" + CONTROLLER + "/slapi/nodes"
r = requests.get(url, headers=headers, verify=False)
hvs = r.json()
if len(hvs["nodes"]) == 0:
    print("Error: hypervisors do not exist")
    sys.exit(1)

local_conf_tpl = """
[[local|localrc]]
ADMIN_PASSWORD=$os_password
DATABASE_PASSWORD=$os_password
RABBIT_PASSWORD=$os_password
SERVICE_PASSWORD=$os_password

[[post-config|$$NOVA_CONF]]
[DEFAULT]
verbose = True
debug = False
compute_driver = slonova.SlapiDriver
allow_resize_to_same_host=true
allow_migrate_to_same_host=true
scheduler_default_filters=AllHostsFilter

[slapi]
connection = $controller
username = $sl_username
password = $sl_password

[[post-config|$$NOVA_CPU_CONF]]
[DEFAULT]
verbose = True
debug = False
compute_driver = slonova.SlapiDriver
allow_resize_to_same_host=true
allow_migrate_to_same_host=true
scheduler_default_filters=AllHostsFilter

[slapi]
connection = $controller
username = $sl_username
password = $sl_password

[[post-config|$$CINDER_CONF]]
[DEFAULT]
verbose = True
debug = False
scheduler_default_filters=AvailabilityZoneFilter
scheduler_default_weighers=CapacityWeigher

[lvmdriver-1]
image_volume_cache_enabled = True
volume_clear = zero
lvm_type = default
iscsi_helper = tgtadm
volume_driver = slocinder.SlapiDriver
volume_backend_name = lvmdriver-1
volume_group = cinder-volumes

[slapi]
connection = $controller
username = $sl_username
password = $sl_password
"""

print("Create the local.conf")
local_conf = Template(local_conf_tpl).substitute(
    os_password=OS_PASSWORD, controller=CONTROLLER,
    sl_username=SL_USERNAME, sl_password=SL_PASSWORD)
fd = os.open(DEVSTACK_FOLDER+"/local.conf", os.O_RDWR | os.O_CREAT)
os.write(fd, local_conf)
os.close(fd)

print("Install the drivers")
os.system("python "+SCRIPT_FOLDER+"/setup.py install --record files.text")
os.system(SCRIPT_FOLDER + '/iscripts/upload_nova_driver.py')

print("Install the devstack")
os.system("sudo -u stack " + DEVSTACK_FOLDER + '/stack.sh')

print("Install the nbd-client")
os.system("wget http://launchpadlibrarian.net/166862192/nbd-client_3.7-1_amd64.deb -O /tmp/nbd-client_3.7-1_amd64.deb")
os.system("sudo dpkg -i /tmp/nbd-client_3.7-1_amd64.deb")
os.system("sudo apt-mark hold nbd-client")

print("Edit the volume.filters")
fd = open("/opt/stack/cinder/etc/cinder/rootwrap.d/volume.filters", "a+")
fd.write("\nnbd-client: CommandFilter, nbd-client, root\n")
fd.close()
os.system("sed -i 's/socket-timeout.*/socket-timeout = 300/g' /etc/glance/glance-uwsgi.ini")
os.system("sudo echo 'nbd-client: CommandFilter, nbd-client, root' >> /etc/cinder/rootwrap.d/volume.filters")

print("The system is ready.")

print("Create networks")
url = "https://" + CONTROLLER + "/slapi/networks"
r = requests.get(url, headers=headers, verify=False)
networks = r.json()

username = 'admin'
password = OS_PASSWORD
project_name = 'demo'
project_domain_id = 'default'
user_domain_id = 'default'
auth_url = 'http://localhost/identity'
auth = identity.Password(auth_url=auth_url,
                         username=username,
                         password=password,
                         project_name=project_name,
                         project_domain_id=project_domain_id,
                         user_domain_id=user_domain_id)
sess = session.Session(auth=auth)
neutron = client.Client(session=sess)

for network in networks:
    new_network_body = {'name': network['name'], 'shared': True}
    new_network = neutron.create_network({'network': new_network_body})
    new_subnet_body = {
        'name': 'Nexvisor Virtual Network ID ' + network['vnet_id'],
        "network_id": new_network['network']['id'],
        "ip_version": network['ip_version'],
        'cidr': network['cidr'],
        'allocation_pools': [{'start': network['ip_start'], 'end': network['ip_end']}],
        'enable_dhcp': False,
        'gateway_ip': network['gateway'],
    }
    neutron.create_subnet({"subnet": new_subnet_body})
