"""
Copyright (c) 2019 Sunlight.io Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License."""

import collections
import contextlib
import copy
import os

from oslo_log import log as logging
from oslo_serialization import jsonutils
from oslo_utils import versionutils
from oslo_utils import excutils
from oslo_config import cfg

from nova.compute import power_state
from nova.compute import task_states
from nova.compute import vm_states
from nova import context as nova_context
from nova import network
from neutronclient.v2_0 import client as neutron_client
from nova.api.metadata import base as instance_metadata

import nova.conf
from nova.console import type as ctype
from nova import exception
from nova import version
from nova.i18n import _

from nova.objects import diagnostics as diagnostics_obj
from nova.objects import fields as obj_fields
from nova.objects import migrate_data
from nova.virt import driver
from nova.virt import hardware
from nova.virt import virtapi
from nova.virt import configdrive
from nova import utils

from slolibrary import http_client

CONF = nova.conf.CONF
LOG = logging.getLogger(__name__)


class SunlightInstance(object):

    def __init__(self, name, state, uuid):
        self.name = name
        self.state = state
        self.uuid = uuid

    def __getitem__(self, key):
        return getattr(self, key)


class Resources(object):
    vcpus = 0
    memory_mb = 0
    local_gb = 0
    vcpus_used = 0
    memory_mb_used = 0
    local_gb_used = 0

    def __init__(self, vcpus=8, memory_mb=8000, local_gb=500):
        self.vcpus = vcpus
        self.memory_mb = memory_mb
        self.local_gb = local_gb

    def claim(self, vcpus=0, mem=0, disk=0):
        self.vcpus_used += vcpus
        self.memory_mb_used += mem
        self.local_gb_used += disk

    def release(self, vcpus=0, mem=0, disk=0):
        self.vcpus_used -= vcpus
        self.memory_mb_used -= mem
        self.local_gb_used -= disk

    def dump(self):
        return {
            'vcpus': self.vcpus,
            'memory_mb': self.memory_mb,
            'local_gb': self.local_gb,
            'vcpus_used': self.vcpus_used,
            'memory_mb_used': self.memory_mb_used,
            'local_gb_used': self.local_gb_used
        }


class SlapiDriver(driver.ComputeDriver):
    capabilities = {
        "has_imagecache": True,
        "supports_recreate": True,
        "supports_migrate_to_same_host": True,
        "supports_attach_interface": True,
        "supports_tagged_attach_interface": True,
        "supports_tagged_attach_volume": True,
        "supports_extend_volume": True,
    }

    # Since we don't have a real hypervisor, pretend we have lots of
    # disk and ram so this driver can be used to test large instances.
    vcpus = 1000
    memory_mb = 800000
    local_gb = 600000

    """Sunlight hypervisor driver."""

    def __init__(self, virtapi, read_only=False):
        super(SlapiDriver, self).__init__(virtapi)
        LOG.info("SLAPI DRIVER INITIALIZED")
        self.instances = {}
        self.resources = Resources(
            vcpus=self.vcpus,
            memory_mb=self.memory_mb,
            local_gb=self.local_gb)
        self.host_status_base = {
            'hypervisor_type': 'Sunlight',
            'hypervisor_version': versionutils.convert_version_to_int('1.0'),
            'hypervisor_hostname': CONF.host,
            'cpu_info': {},
            'disk_available_least': 0,
            'supported_instances': [(
                obj_fields.Architecture.X86_64,
                obj_fields.HVType.FAKE,
                obj_fields.VMMode.HVM)],
            'numa_topology': None,
        }
        self._mounts = {}
        self._interfaces = {}
        self.active_migrations = {}
        self._nodes = []
        self._ctxt = nova_context.get_admin_context()

    def init_host(self, host):
        LOG.info("CALL: init_host")
        return

    def list_instances(self):
        LOG.info("CALL: list_instances")
        return [self.instances[uuid].name for uuid in self.instances.keys()]

    def list_instance_uuids(self):
        LOG.info("CALL: list_instance_uuids")
        return list(self.instances.keys())

    def plug_vifs(self, instance, network_info):
        LOG.info("CALL: plug_vifs")
        """Plug VIFs into networks."""
        pass

    def unplug_vifs(self, instance, network_info):
        """Unplug VIFs from networks."""
        LOG.info("CALL: unplug_vifs")
        pass

    def _get_instance_metadata(self, context, instance):
        flavor = instance.flavor
        metadata = [('name', instance.display_name),
                    ('userid', context.user_id),
                    ('username', context.user_name),
                    ('projectid', context.project_id),
                    ('projectname', context.project_name),
                    ('flavor:name', flavor.name),
                    ('flavor:memory_mb', flavor.memory_mb),
                    ('flavor:vcpus', flavor.vcpus),
                    ('flavor:ephemeral_gb', flavor.ephemeral_gb),
                    ('flavor:root_gb', flavor.root_gb),
                    ('flavor:swap', flavor.swap),
                    ('imageid', instance.image_ref),
                    ('package', version.version_string_with_package())]
        # NOTE: formatted as lines like this: 'name:NAME\nuserid:ID\n...'
        return ''.join(['%s:%s\n' % (k, v) for k, v in metadata])

    def _create_config_drive(self, context, instance, injected_files,
                             admin_password, network_info):
        if CONF.config_drive_format != 'iso9660':
            reason = (_('Invalid config_drive_format "%s"') %
                      CONF.config_drive_format)
            raise exception.InstancePowerOnFailure(reason=reason)

        LOG.info('Using config drive for instance', instance=instance)
        extra_md = {}
        if admin_password:
            extra_md['admin_pass'] = admin_password

        inst_md = instance_metadata.InstanceMetadata(instance,
                                                     content=injected_files,
                                                     extra_md=extra_md,
                                                     network_info=network_info,
                                                     request_context=context)
        try:
            with configdrive.ConfigDriveBuilder(instance_md=inst_md) as cdb:
                with utils.tempdir() as tmp_path:
                    tmp_file = os.path.join(tmp_path, 'configdrive.iso')
                    cdb.make_drive(tmp_file)
                    f = open(tmp_file, 'rb')
                    return f.read()
        except Exception as e:
            with excutils.save_and_reraise_exception():
                LOG.error('Creating config drive failed with error: %s',
                          e, instance=instance)

    def spawn(self, context, instance, image_meta, injected_files,
              admin_password, network_info=None, block_device_info=None):
        LOG.info("CALL: spawn")
        LOG.info("Instance: " + str(instance))
        LOG.info("Image meta: " + str(image_meta))
        LOG.info("Network info: " + str(network_info))
        LOG.info("Block device info: " + str(block_device_info))
        uuid = instance.uuid
        state = power_state.RUNNING
        flavor = instance.flavor
        self.resources.claim(
            vcpus=flavor.vcpus,
            mem=flavor.memory_mb,
            disk=flavor.root_gb)
        self.instances[uuid] = SunlightInstance(instance.name, state, uuid)
        volumes = []
        for block in block_device_info['block_device_mapping']:
            volume = {
                "name": block['connection_info']['serial'][:27],
                "delete_on_termination": block['delete_on_termination']
            }
            volumes.append(volume)

        config_bytes = self._create_config_drive(context, instance,
                                                 injected_files, admin_password,
                                                 network_info)
        ips = []
        for net in network_info:
            ip = {'mac': net['address'], 'address': net['network']
                  ['subnets'][0]['ips'][0]['address']}
            ips.append(ip)

        res = http_client.create_instance(
            uuid, flavor.memory_mb,
            flavor.vcpus,
            instance.node,
            ips,
            volumes,
            config_bytes)
        LOG.info("finished spawning the instance " + str(res))

    def snapshot(self, context, instance, image_id, update_task_state):
        LOG.info("CALL: snapshot")
        if instance.uuid not in self.instances:
            raise exception.InstanceNotRunning(instance_id=instance.uuid)
        update_task_state(task_state=task_states.IMAGE_UPLOADING)

    def reboot(self, context, instance, network_info, reboot_type,
               block_device_info=None, bad_volumes_callback=None):
        LOG.info("CALL: reboot")
        http_client.act_on_instance(instance.uuid, http_client.ACTION_REBOOT)

    def get_host_ip_addr(self):
        LOG.info("CALL: get_host_ip_addr")
        return '192.168.0.1'

    def set_admin_password(self, instance, new_pass):
        LOG.info("CALL: set_admin_password")
        pass

    def inject_file(self, instance, b64_path, b64_contents):
        LOG.info("CALL: inject_file")
        pass

    def resume_state_on_host_boot(self, context, instance, network_info,
                                  block_device_info=None):
        LOG.info("CALL: resume_state_on_host_boot")
        pass

    def rescue(self, context, instance, network_info, image_meta,
               rescue_password):
        LOG.info("CALL: rescue")
        pass

    def unrescue(self, instance, network_info):
        LOG.info("CALL: unrescue")
        pass

    def poll_rebooting_instances(self, timeout, instances):
        LOG.info("CALL: poll_rebooting_instances")
        pass

    def migrate_disk_and_power_off(self, context, instance, dest,
                                   flavor, network_info,
                                   block_device_info=None,
                                   timeout=0, retry_interval=0):
        LOG.info("CALL: migrate_disk_and_power_off")
        pass

    def finish_revert_migration(self, context, instance, network_info,
                                block_device_info=None, power_on=True):
        self.instances[instance.uuid] = SunlightInstance(
            instance.name, power_state.RUNNING, instance.uuid)
        LOG.info("CALL: finish_revert_migration")

    def post_live_migration_at_destination(self, context, instance,
                                           network_info,
                                           block_migration=False,
                                           block_device_info=None):
        LOG.info("CALL: post_live_migration_at_destination")
        pass

    def power_off(self, instance, timeout=0, retry_interval=0):
        LOG.info("CALL: power_off")
        info = http_client.get_info_instance(instance.uuid)
        if info['state'] != power_state.SHUTDOWN:
            http_client.act_on_instance(instance.uuid, http_client.ACTION_STOP)

    def power_on(self, context, instance, network_info,
                 block_device_info=None):
        LOG.info("CALL: power_on")
        info = http_client.get_info_instance(instance.uuid)
        if info['state'] != power_state.RUNNING:
            http_client.act_on_instance(
                instance.uuid, http_client.ACTION_START)

    def trigger_crash_dump(self, instance):
        LOG.info("CALL: trigger_crash_dump")
        pass

    def soft_delete(self, instance):
        LOG.info("CALL: soft_delete")
        pass

    def restore(self, instance):
        LOG.info("CALL: restore")
        pass

    def pause(self, instance):
        LOG.info("CALL: pause")
        http_client.act_on_instance(instance.uuid, http_client.ACTION_PAUSE)

    def unpause(self, instance):
        LOG.info("CALL: unpause")
        http_client.act_on_instance(instance.uuid, http_client.ACTION_UNPAUSE)

    def suspend(self, context, instance):
        LOG.info("CALL: suspend")
        pass

    def resume(self, context, instance, network_info, block_device_info=None):
        LOG.info("CALL: resume")
        pass

    def destroy(self, context, instance, network_info, block_device_info=None,
                destroy_disks=True):
        LOG.info("CALL: destroy")
        try:
            http_client.act_on_instance(
                instance.uuid, http_client.ACTION_DESTROY)
        except http_client.NotFoundException:
            LOG.error("The instance " + instance.uuid + " does not exists.")
            
        flavor = instance.flavor
        self.resources.release(
            vcpus=flavor.vcpus,
            mem=flavor.memory_mb,
            disk=flavor.root_gb)
        key = instance.uuid
        if key in self.instances:
            del self.instances[key]
        else:
            LOG.warning("Key '%(key)s' not in instances '%(inst)s'",
                        {'key': key,
                         'inst': self.instances}, instance=instance)

    def cleanup(self, context, instance, network_info, block_device_info=None,
                destroy_disks=True, migrate_data=None, destroy_vifs=True):
        LOG.info("CALL: cleanup")
        pass

    def attach_volume(self, context, connection_info, instance, mountpoint,
                      disk_bus=None, device_type=None, encryption=None):
        """Attach the disk to the instance at mountpoint using info."""
        LOG.info("CALL: attach_volume")
        instance_name = instance.name
        if instance_name not in self._mounts:
            self._mounts[instance_name] = {}
        self._mounts[instance_name][mountpoint] = connection_info

    def detach_volume(self, connection_info, instance, mountpoint,
                      encryption=None):
        """Detach the disk attached to the instance."""
        LOG.info("CALL: detach_volume")
        try:
            del self._mounts[instance.name][mountpoint]
        except KeyError:
            pass

    def swap_volume(self, context, old_connection_info, new_connection_info,
                    instance, mountpoint, resize_to):
        """Replace the disk attached to the instance."""
        LOG.info("CALL: swap_volume")
        instance_name = instance.name
        if instance_name not in self._mounts:
            self._mounts[instance_name] = {}
        self._mounts[instance_name][mountpoint] = new_connection_info

    def extend_volume(self, connection_info, instance):
        """Extend the disk attached to the instance."""
        LOG.info("CALL: extend_volume")
        pass

    def attach_interface(self, context, instance, image_meta, vif):
        LOG.info("CALL: attach_interface")
        if vif['id'] in self._interfaces:
            raise exception.InterfaceAttachFailed(
                instance_uuid=instance.uuid)
        self._interfaces[vif['id']] = vif

    def detach_interface(self, context, instance, vif):
        LOG.info("CALL: detach_interface")
        try:
            del self._interfaces[vif['id']]
        except KeyError:
            raise exception.InterfaceDetachFailed(
                instance_uuid=instance.uuid)

    def get_info(self, instance):
        LOG.info("CALL: get_info")
        try:
            info = http_client.get_info_instance(instance.uuid)
            return hardware.InstanceInfo(state=info['state'],
                                         max_mem_kb=info['max_mem_kb'],
                                         mem_kb=info['mem_kb'],
                                         num_cpu=info['num_cpu'],
                                         cpu_time_ns=info['cpu_time_ns'])
        except http_client.NotFoundException:
            raise exception.InstanceNotFound(instance_id=instance.uuid)

    def get_diagnostics(self, instance):
        LOG.info("CALL: get_diagnostics")
        return {'cpu0_time': 17300000000,
                'memory': 524288,
                'vda_errors': -1,
                'vda_read': 262144,
                'vda_read_req': 112,
                'vda_write': 5778432,
                'vda_write_req': 488,
                'vnet1_rx': 2070139,
                'vnet1_rx_drop': 0,
                'vnet1_rx_errors': 0,
                'vnet1_rx_packets': 26701,
                'vnet1_tx': 140208,
                'vnet1_tx_drop': 0,
                'vnet1_tx_errors': 0,
                'vnet1_tx_packets': 662,
                }

    def get_instance_diagnostics(self, instance):
        LOG.info("CALL: get_instance_diagnostics")
        diags = diagnostics_obj.Diagnostics(
            state='running', driver='libvirt', hypervisor='kvm',
            hypervisor_os='ubuntu', uptime=46664, config_drive=True)
        diags.add_cpu(id=0, time=17300000000, utilisation=15)
        diags.add_nic(mac_address='01:23:45:67:89:ab',
                      rx_octets=2070139,
                      rx_errors=100,
                      rx_drop=200,
                      rx_packets=26701,
                      rx_rate=300,
                      tx_octets=140208,
                      tx_errors=400,
                      tx_drop=500,
                      tx_packets=662,
                      tx_rate=600)
        diags.add_disk(read_bytes=262144,
                       read_requests=112,
                       write_bytes=5778432,
                       write_requests=488,
                       errors_count=1)
        diags.memory_details = diagnostics_obj.MemoryDiagnostics(
            maximum=524288, used=0)
        return diags

    def get_all_bw_counters(self, instances):
        LOG.info("CALL: get_all_bw_counters")
        """Return bandwidth usage counters for each interface on each
           running VM.
        """
        bw = []
        for instance in instances:
            bw.append({'uuid': instance.uuid,
                       'mac_address': 'fa:16:3e:4c:2c:30',
                       'bw_in': 0,
                       'bw_out': 0})
        return bw

    def get_all_volume_usage(self, context, compute_host_bdms):
        LOG.info("CALL: get_all_volume_usage")
        """Return usage info for volumes attached to vms on
           a given host.
        """
        volusage = []
        return volusage

    def get_host_cpu_stats(self):
        LOG.info("CALL: get_host_cpu_stats")
        stats = {'kernel': 5664160000000,
                 'idle': 1592705190000000,
                 'user': 26728850000000,
                 'iowait': 6121490000000}
        stats['frequency'] = 800
        return stats

    def block_stats(self, instance, disk_id):
        LOG.info("CALL: block_stats")
        return [0, 0, 0, 0, None]

    def get_console_output(self, context, instance):
        LOG.info("CALL: get_console_output")
        return 'SLAPI CONSOLE OUTPUT\nANOTHER\nLAST LINE'

    def get_vnc_console(self, context, instance):
        LOG.info("CALL: get_vnc_console")
        return ctype.ConsoleVNC(internal_access_path='SLAPI',
                                host='slvncconsole.com',
                                port=6969)

    def get_spice_console(self, context, instance):
        LOG.info("CALL: get_spice_console")
        return ctype.ConsoleSpice(internal_access_path='SLAPI',
                                  host='slspiceconsole.com',
                                  port=6969,
                                  tlsPort=6970)

    def get_rdp_console(self, context, instance):
        LOG.info("CALL: get_rdp_console")
        return ctype.ConsoleRDP(internal_access_path='SLAPI',
                                host='slrdpconsole.com',
                                port=6969)

    def get_serial_console(self, context, instance):
        LOG.info("CALL: get_serial_console")
        return ctype.ConsoleSerial(internal_access_path='SLAPI',
                                   host='slrdpconsole.com',
                                   port=6969)

    def get_mks_console(self, context, instance):
        LOG.info("CALL: get_mks_console")
        return ctype.ConsoleMKS(internal_access_path='SLAPI',
                                host='slmksconsole.com',
                                port=6969)

    def get_console_pool_info(self, console_type):
        LOG.info("CALL: get_console_pool_info")
        return {'address': '127.0.0.1',
                'username': 'sluser',
                'password': 'slpassword'}

    def refresh_security_group_rules(self, security_group_id):
        LOG.info("CALL: refresh_security_group_rules")
        return True

    def refresh_instance_security_rules(self, instance):
        LOG.info("CALL: refresh_instance_security_rules")
        return True

    def get_available_resource(self, nodename):
        LOG.info("CALL: get_available_resource")
        """Updates compute manager resource info on ComputeNode table.

           Since we don't have a real hypervisor, pretend we have lots of
           disk and ram.
        """
        cpu_info = collections.OrderedDict([
            ('arch', 'x86_64'),
            ('model', 'Nehalem'),
            ('vendor', 'Intel'),
            ('features', ['pge', 'clflush']),
            ('topology', {
                'cores': 1,
                'threads': 1,
                'sockets': 4,
            }),
        ])
        if nodename not in self._nodes:
            return {}

        LOG.info("CALLS: get_available_resource " + nodename)
        res = http_client.get_resources_by_node(nodename)

        host_status = self.host_status_base.copy()
        host_status.update(res)
        host_status['hypervisor_hostname'] = nodename
        host_status['host_hostname'] = nodename
        host_status['host_name_label'] = nodename
        host_status['cpu_info'] = jsonutils.dumps(cpu_info)
        return host_status

    def ensure_filtering_rules_for_instance(self, instance, network_info):
        LOG.info("CALL: ensure_filtering_rules_for_instance")
        return

    def get_instance_disk_info(self, instance, block_device_info=None):
        LOG.info("CALL: get_instance_disk_info")
        return

    def live_migration(self, context, instance, dest,
                       post_method, recover_method, block_migration=False,
                       migrate_data=None):
        LOG.info("CALL: live_migration")
        post_method(context, instance, dest, block_migration,
                    migrate_data)
        return

    def live_migration_force_complete(self, instance):
        LOG.info("CALL: live_migration_force_complete")
        return

    def live_migration_abort(self, instance):
        LOG.info("CALL: live_migration_abort")
        return

    def cleanup_live_migration_destination_check(self, context,
                                                 dest_check_data):
        LOG.info("CALL: cleanup_live_migration_destination_check")
        return

    def check_can_live_migrate_destination(self, context, instance,
                                           src_compute_info, dst_compute_info,
                                           block_migration=False,
                                           disk_over_commit=False):
        LOG.info("CALL: check_can_live_migrate_destination")
        data = migrate_data.LibvirtLiveMigrateData()
        data.filename = 'slapi'
        data.image_type = CONF.libvirt.images_type
        data.graphics_listen_addr_vnc = CONF.vnc.vncserver_listen
        data.graphics_listen_addr_spice = CONF.spice.server_listen
        data.serial_listen_addr = None
        # Notes(eliqiao): block_migration and disk_over_commit are not
        # nullable, so just don't set them if they are None
        if block_migration is not None:
            data.block_migration = block_migration
        if disk_over_commit is not None:
            data.disk_over_commit = disk_over_commit
        data.disk_available_mb = 100000
        data.is_shared_block_storage = True
        data.is_shared_instance_path = True

        return data

    def check_can_live_migrate_source(self, context, instance,
                                      dest_check_data, block_device_info=None):
        LOG.info("CALL: check_can_live_migrate_source")
        return dest_check_data

    def finish_migration(self, context, migration, instance, disk_info,
                         network_info, image_meta, resize_instance,
                         block_device_info=None, power_on=True):
        LOG.info("CALL: finish_migration")
        LOG.info("Migration :" + str(migration))
        LOG.info("Instance :" + str(instance))
        LOG.info("Disk info :" + str(disk_info))
        LOG.info("Resize instance : " + str(resize_instance))
        LOG.info("Block device info : " + str(block_device_info))
        # info = http_client.get_info_instance(instance.uuid)
        # if info['state'] != power_state.SHUTDOWN:
        #    http_client.act_on_instance(instance.uuid, http_client.ACTION_STOP)

    def confirm_migration(self, context, migration, instance, network_info):
        LOG.info("CALL: confirm_migration")
        # Confirm migration cleans up the guest from the source host so just
        # destroy the guest to remove it from the list of tracked instances
        # unless it is a same-host resize.
        if migration.source_node != migration.dest_node:
            restart = instance.power_state == power_state.RUNNING
            http_client.act_on_instance(
                instance.uuid, http_client.ACTION_MIGRATE, migration.dest_node, restart)

    def pre_live_migration(self, context, instance, block_device_info,
                           network_info, disk_info, migrate_data):
        LOG.info("CALL: pre_live_migration")
        return migrate_data

    def unfilter_instance(self, instance, network_info):
        LOG.info("CALL: unfilter_instance")
        return

    def _test_remove_vm(self, instance_uuid):
        LOG.info("CALL: _test_remove_vm")
        """Removes the named VM, as if it crashed. For testing."""
        self.instances.pop(instance_uuid)

    def host_power_action(self, action):
        LOG.info("CALL: host_power_action")
        """Reboots, shuts down or powers up the host."""
        return action

    def host_maintenance_mode(self, host, mode):
        LOG.info("CALL: host_maintenance_mode")
        """Start/Stop host maintenance window. On start, it triggers
        guest VMs evacuation.
        """
        if not mode:
            return 'off_maintenance'
        return 'on_maintenance'

    def set_host_enabled(self, enabled):
        LOG.info("CALL: set_host_enabled")
        """Sets the specified host's ability to accept new instances."""
        if enabled:
            return 'enabled'
        return 'disabled'

    def get_volume_connector(self, instance):
        LOG.info("CALL: get_volume_connector")
        return {'ip': CONF.my_block_storage_ip,
                'initiator': 'sunlight',
                'host': 'slhost'}

    def get_available_nodes(self, refresh=False):
        LOG.info("CALL: get_available_nodes")
        res = http_client.get_nodes()
        self._nodes = res['nodes']
        return self._nodes

    def instance_on_disk(self, instance):
        LOG.info("CALL: instance_on_disk")
        return False

    def quiesce(self, context, instance, image_meta):
        LOG.info("CALL: quiesce")
        pass

    def unquiesce(self, context, instance, image_meta):
        LOG.info("CALL: unquiesce")
        pass


class SunlightVirtAPI(virtapi.VirtAPI):
    @contextlib.contextmanager
    def wait_for_instance_event(self, instance, event_names, deadline=300,
                                error_callback=None):
        # NOTE(danms): Don't actually wait for any events, just
        # fall through
        yield


class SmallSunlightDriver(SlapiDriver):
    # The api samples expect specific cpu memory and disk sizes. In order to
    # allow the SunlightVirt driver to be used outside of the unit tests, provide
    # a separate class that has the values expected by the api samples. So
    # instead of requiring new samples every time those
    # values are adjusted allow them to be overwritten here.

    vcpus = 1
    memory_mb = 8192
    local_gb = 1028


class MediumSunlightDriver(SlapiDriver):
    # Sunlight driver that has enough resources to host more than one instance
    # but not that much that cannot be exhausted easily

    vcpus = 10
    memory_mb = 8192
    local_gb = 1028


class SunlightRescheduleDriver(SlapiDriver):
    """SunlightDriver derivative that triggers a reschedule on the first spawn
    attempt. This is expected to only be used in tests that have more than
    one compute service.
    """
    # dict, keyed by instance uuid, mapped to a boolean telling us if the
    # instance has been rescheduled or not
    rescheduled = {}

    def spawn(self, context, instance, image_meta, injected_files,
              admin_password, network_info=None, block_device_info=None):
        if not self.rescheduled.get(instance.uuid, False):
            # We only reschedule on the first time something hits spawn().
            self.rescheduled[instance.uuid] = True
            raise exception.ComputeResourcesUnavailable(
                reason='SunlightRescheduleDriver')
        super(SunlightRescheduleDriver, self).spawn(
            context, instance, image_meta, injected_files,
            admin_password, network_info, block_device_info)


class SunlightBuildAbortDriver(SlapiDriver):
    """SunlightDriver derivative that always fails on spawn() with a
    BuildAbortException so no reschedule is attempted.
    """

    def spawn(self, context, instance, image_meta, injected_files,
              admin_password, network_info=None, block_device_info=None):
        raise exception.BuildAbortException(
            instance_uuid=instance.uuid, reason='SunlightBuildAbortDriver')


class SunlightUnshelveSpawnFailDriver(SlapiDriver):
    """SunlightDriver derivative that always fails on spawn() with a
    VirtualInterfaceCreateException when unshelving an offloaded instance.
    """

    def spawn(self, context, instance, image_meta, injected_files,
              admin_password, network_info=None, block_device_info=None):
        if instance.vm_state == vm_states.SHELVED_OFFLOADED:
            raise exception.VirtualInterfaceCreateException(
                'SunlightUnshelveSpawnFailDriver')
        # Otherwise spawn normally during the initial build.
        super(SunlightUnshelveSpawnFailDriver, self).spawn(
            context, instance, image_meta, injected_files,
            admin_password, network_info, block_device_info)
