"""
Copyright (c) 2019 Sunlight.io Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License."""

import requests
import time
import json
import base64
from oslo_log import log as logging
from oslo_config import cfg
CONF = cfg.CONF
LOG = logging.getLogger(__name__)


slapi_opts = [
    cfg.StrOpt('connection',
               default='127.0.0.1',
               help='IP address for connecting to SLAPI. '),
    cfg.StrOpt('username',
               help='Username for Sunlight API authorization'),
    cfg.StrOpt('password',
               help='Password for Sunlight API authorization'),
]
CONF.register_opts(slapi_opts, 'slapi')

USERNAME = CONF.slapi.username
PASSWORD = CONF.slapi.password
URL = "https://"+CONF.slapi.connection
GLOBAL_AUTH_HEADER = {}

ACTION_START = "start"
ACTION_STOP = "stop"
ACTION_DESTROY = "destroy"
ACTION_PAUSE = "pause"
ACTION_REBOOT = "reboot"
ACTION_UNPAUSE = "unpause"
ACTION_MIGRATE = "migrate"


class UnauthorizedExceptionFail(Exception):
    LOG.info("FAILURE to access SLAPI after 3 retries!")
    pass

class UnauthorizedException(Exception):
    pass


def decorator_login(times=3):
    def wrap(func):
        LOG.info("Start retries " + str(times))

        def func_wrapper(*args, **kwargs):
            res = False
            tries = times
            while tries > 0:
                try:
                    res = func(*args, **kwargs)
                except UnauthorizedException:
                    tries -= 1
                    LOG.info("Retries login left " + str(tries))
                    payload = {'username': USERNAME, 'password': PASSWORD}
                    r = requests.post(URL + "/auth/login",
                                      json=payload, verify=False)
                    user = r.json()
                    token = user["jwt"]
                    GLOBAL_AUTH_HEADER['Authorization'] = "Bearer " + token
                else:
                    return res
            if tries <= 0:
                raise UnauthorizedExceptionFail

            return func(*args, **kwargs)

        return func_wrapper

    return wrap


@decorator_login()
def create_instance(name, ram, vcpus, node_id, ips=[], volumes=[], config_driver=[]):
    url = URL + "/slapi/instances"
    data = {'name': name, 'ram': ram, 'node_id': node_id,
            'vcpus': vcpus, 'volumes': volumes, 'ips': ips,
            "configdriver": base64.b64encode(config_driver)}
    LOG.info("curl -d '" + "' -X POST " + url)
    r = requests.post(url, json.dumps(
        data), headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code >= 400:
        raise Exception(r.text)

    return r.json()


@decorator_login()
def act_on_instance(name, action, node_id=None, restart_after=False):
    url = URL + "/slapi/instances/"+name+"/action"
    data = {'name': name, "action": action}
    if node_id:
        data['node_id'] = node_id

    data['restart_after'] = restart_after
    LOG.info("curl -d '" + json.dumps(data) + "' -X PUT " + url)
    r = requests.put(url, json.dumps(
        data), headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code == 404:
        raise NotFoundException("Instance", name)

    if r.status_code >= 400:
        raise Exception(r.text)


@decorator_login()
def get_info_instance(name):
    url = URL + "/slapi/instances/"+name+"/info"
    LOG.info("curl -X GET " + url)
    r = requests.get(url, headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code == 404:
        raise NotFoundException("Instance", name)

    if r.status_code >= 400:
        raise Exception(r.text)

    return r.json()


@decorator_login()
def get_nodes():
    url = URL + "/slapi/nodes"
    LOG.info("curl -X GET " + url)
    r = requests.get(url, headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    return r.json()


@decorator_login()
def get_resources_by_node(node_id):
    url = URL + "/slapi/nodes/" + node_id + "/resources"
    LOG.info("curl -X GET " + url)
    r = requests.get(url, headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code == 404:
        raise NotFoundException("Node", node_id)

    return r.json()


@decorator_login()
def create_volume(name, size):
    url = URL + "/slapi/volumes"
    data = {'name': name, 'size': size}
    LOG.info("curl -d '" + json.dumps(data) + "' -X POST " + url)
    r = requests.post(url, json.dumps(
        data), headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code >= 400:
        raise Exception(r.text)

    return r.json()


@decorator_login()
def attach_nbd_volume(name):
    url = URL + "/slapi/volumes/"+name+"/nbd"
    LOG.info("curl -X POST " + url)
    r = requests.post(url, headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code >= 400:
        raise Exception(r.text)

    return r.json()


@decorator_login()
def detach_nbd_volume(name):
    url = URL + "/slapi/volumes/"+name+"/nbd"
    LOG.info("curl -X DELETE " + url)
    r = requests.delete(url, headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code >= 400:
        raise Exception(r.text)


@decorator_login()
def delete_volume(name):
    url = URL + "/slapi/volumes/"+name
    LOG.info("curl -X DELETE " + url)
    r = requests.delete(url, headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code == 404:
        raise NotFoundException("Volume", name)

    if r.status_code >= 400:
        raise Exception(r.text)


@decorator_login()
def clone_volume(source_name, dest_name, dest_size):
    url = URL + "/slapi/volumes/"+source_name+"/clone"
    data = {'name': dest_name, 'size': dest_size}
    LOG.info("curl -d '" + json.dumps(data) + "' -X PUT " + url)
    r = requests.put(url, json.dumps(
        data), headers=GLOBAL_AUTH_HEADER, verify=False)
    if r.status_code == 401:
        raise UnauthorizedException()

    if r.status_code == 404:
        raise NotFoundException("Volume", source_name)

    if r.status_code >= 400:
        raise Exception(r.text)

    return r.json()


class NotFoundException(Exception):
    def __init__(self, type_name, value):
        self.value = value
        self.type_name = type_name

    def __str__(self):
        return self.type_name + " with ID " + self.value + " not found"
