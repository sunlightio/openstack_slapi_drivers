#!/bin/bash

sudo systemctl restart "devstack@c-vol.service" 
sudo systemctl restart "devstack@c-sch.service"
sudo systemctl restart "devstack@c-api.service"  

sudo systemctl restart "devstack@n-cpu.service"
sudo systemctl restart "devstack@n-cond-cell1.service"
sudo systemctl restart "devstack@n-super-cond.service"
sudo systemctl restart "devstack@n-sch.service"
sudo systemctl restart "devstack@n-api.service"

sudo systemctl restart "devstack@q-agt.service"
sudo systemctl restart "devstack@q-dhcp.service"
sudo systemctl restart "devstack@q-l3.service"
sudo systemctl restart "devstack@q-meta.service"
sudo systemctl restart "devstack@q-svc.service"
